import { Children, useEffect, useState } from "react";
import { Vortex } from "react-loader-spinner";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const options = {
  method: "GET",
  headers: {
    "X-RapidAPI-Key": "c3022f1ae5msh0ca4f06933c0d39p16b3a4jsned7531e4def1",
    "X-RapidAPI-Host": "deezerdevs-deezer.p.rapidapi.com",
  },
};

export default function App() {
  const [foundTracks, setFoundTracks] = useState([]);
  const [likedTracks, setLikedTrack] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [searchTrack, setSearchTrack] = useState("");

  async function fetchTracks() {
    if (searchTrack === "" || searchTrack === " ") {
      setFoundTracks([]);
      return;
    }
    try {
      setError("");
      setIsLoading(true);

      let res = await fetch(
        `https://deezerdevs-deezer.p.rapidapi.com/search?q=${searchTrack}`,
        options
      );

      // on connection error
      if (!res.ok) throw new Error("Something went wrong..");

      res = await res.json();

      // on no data found
      if (res.total === 0) throw new Error("No Match found..");

      setFoundTracks(res.data);
    } catch (err) {
      // toast.error(err.message, {
      //   position: "bottom-right",
      //   autoClose: 3000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true,
      //   progress: undefined,
      //   theme: "dark",
      // });
      setError(err.message);
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(
    function () {
      fetchTracks();
    },
    [searchTrack]
  );

  function handleClearSearch() {
    setSearchTrack("");
    setError("");
  }

  function handleSetLikedTracks(id, cover, songName, artistName) {
    const selectedTrack = {
      id,
      songName,
      artistName,
      cover,
    };

    const isDuplicate = likedTracks.some(
      (track) => track.id === selectedTrack.id
    );

    if (!isDuplicate) {
      setLikedTrack([...likedTracks, selectedTrack]);
      toast.success(`${artistName} - ${songName} added.`, {
        position: "bottom-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    } else {
      toast.warn(`${artistName} - ${songName} already added.`, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    }
  }

  return (
    <div className="App">
      <div className="container">
        <Header>
          <Logo>music.app</Logo>
          <SearchBar
            searchTrack={searchTrack}
            onSearchTrack={setSearchTrack}
            onClearSearch={handleClearSearch}
          />
        </Header>
        <Main>
          <ToastContainer />
          <Section>
            <Block>
              <h3>Search Results</h3>
              <Box>
                {isLoading && (
                  <div className="loader">
                    <Vortex
                      visible={true}
                      height="80"
                      width="80"
                      ariaLabel="vortex-loading"
                      wrapperStyle={{}}
                      wrapperClass="vortex-wrapper"
                      colors={[
                        "#4fa94d",
                        "#4fa94d",
                        "#4fa94d",
                        "#4fa94d",
                        "#4fa94d",
                        "#4fa94d",
                      ]}
                    />
                  </div>
                )}
                {!isLoading && !error && (
                  <div>
                    <Results
                      items={foundTracks}
                      type="found"
                      onSetLikedTracks={handleSetLikedTracks}
                    />
                  </div>
                )}
                {error && (
                  <div className="error">
                    <ErrorMessage message={error} />
                  </div>
                )}
              </Box>
            </Block>
            <Block>
              <h3>Liked Songs</h3>
              <Box>
                <Results items={likedTracks} type="liked" />
              </Box>
            </Block>
          </Section>
        </Main>
      </div>
    </div>
  );
}
function Header({ children }) {
  return <header>{children}</header>;
}
function Logo({ children }) {
  return (
    <div className="logo">
      <h2>{children}</h2>
    </div>
  );
}
function SearchBar({ searchTrack, onSearchTrack, onClearSearch }) {
  return (
    <form>
      <input
        type="text"
        className="search"
        placeholder="What would you like to listen?"
        value={searchTrack}
        onChange={(e) => onSearchTrack(e.target.value)}
      />
      {searchTrack && (
        <i class="bi bi-x clear-search" onClick={onClearSearch} />
      )}
    </form>
  );
}
function Main({ children }) {
  return <main>{children}</main>;
}
function Section({ children }) {
  return <section>{children}</section>;
}
function Block({ children }) {
  return <aside className="block">{children}</aside>;
}
function Box({ children }) {
  return <div className="box">{children}</div>;
}
function Results({ items, type, onSetLikedTracks }) {
  return (
    <ul>
      {items.map((item) => (
        <Result
          item={item}
          key={item.id}
          type={type}
          onSetLikedTracks={onSetLikedTracks}
        />
      ))}
    </ul>
  );
}
function Result({ item, type, onSetLikedTracks }) {
  let id,
    cover,
    songName,
    artistName = "";

  if (type === "found") {
    id = item.id;
    cover = item.album.cover;
    songName = item.title_short;
    artistName = item.artist.name;
  }
  if (type === "liked") {
    id = item.id;
    cover = item.cover;
    songName = item.songName;
    artistName = item.artistName;
  }

  return (
    <li className="song-item">
      <div className="song-details">
        <img src={cover} alt={cover} className="cover" />
        <div className="song-and-artist-name">
          <div className="song-name">{songName}</div>
          <div className="artist-name">{artistName}</div>
        </div>
      </div>
      <div className="heart-icon">
        <i
          onClick={() => onSetLikedTracks(id, cover, songName, artistName)}
          className={`bi love ${
            type === "found" ? "bi-heart" : "bi-heart-fill"
          }`}
        ></i>
      </div>
    </li>
  );
}

function ErrorMessage({ message }) {
  return <h2 className="error-message">{message}</h2>;
}
